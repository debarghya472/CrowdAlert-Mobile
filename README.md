# **CrowdAlert**

A crowdsourced hybrid mobile application using React Native for reporting and viewing incidents around the globe.

----

#### Screenshots

<table>
    <tr>
     <td><kbd><img src="./doc/Screenshots/intro-first.png"></kbd></td>
     <td><kbd><img src="./doc/Screenshots/intro-last.png"></kbd></td>
     <td><kbd><img src="./doc/Screenshots/Login.png"></kbd></td>
     <td><kbd><img src="./doc/Screenshots/DashBoard.png"></kbd></td>
     <tr> 
      <td><kbd><img src="./doc/Screenshots/SideNav.png"></kbd></td>
      <td><kbd><img src="./doc/Screenshots/Maps.png"></kbd></td>
      <td><kbd><img src="./doc/Screenshots/Incident.png"></kbd></td>
      <td><kbd><img src="./doc/Screenshots/Settings.png"></kbd></td>
    </tr>
  </table>

----

### Installation

Follow the [Android guide](doc/android_guide.md) and [iOS guide](doc/iOS_guide.md) for setting up the project for the respective platforms.

Follow [this guide](doc/basic_guide.md) for installing basic dependencies for the project like _Node_ / _npm packages_ / _react-native-cli_ etc.

### Keys Setup

Follow the [Keys installation guide](doc/keys.md) for setting up the keys for CrowdAlert-Mobile.

### Error, Fixes & Helps

If you face any error while building the project , then please check if your error and it's solution can be found in this [guide](doc/Error,Fixes&Helps.md).

### Like this project and want to contribute to it??

We welcome all kind of support from you guys. Just have a quick look at the [Contribution guide](doc/contributions.md) for some rules and regulations to be followed before you send a MR.
